<?php

use Slim\App;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;


return function (App $app) {
    
    $app->post('/api/tokens', \App\Action\TokenCreateAction::class);
	$app->get('/api/tokens', \App\Action\TokenCreateAction::class);

	/* Tabla Empresas */
    $app->get('/api/empresa',  \App\Action\Empresa\GetEmpresaAction::class);
	$app->post('/api/empresa/mostrar', \App\Action\Empresa\GetEmpresaByIdAction::class);
	$app->get('/api/empresa/borrar', \App\Action\Empresa\DeleteEmpresaAction::class);
	$app->post('/api/empresa/actualizar', \App\Action\Empresa\UpdateEmpresaAction::class);
	$app->post('/api/empresa/insertar',  \App\Action\Empresa\AddEmpresaAction::class);

	/* Tabla Trabajos */
    $app->get('/api/trabajo',  \App\Action\Trabajo\GetTrabajoAction::class);
    $app->post('/api/trabajo/mapa',  \App\Action\Trabajo\GetTrabajoMapaAction::class);
    $app->get('/api/trabajo/mostrar', \App\Action\Trabajo\GetTrabajoByIdAction::class);
	$app->post('/api/trabajo/usuario', \App\Action\Trabajo\GetTrabajoByEmailAction::class);
	$app->post('/api/trabajo/insertar', \App\Action\Trabajo\AddTrabajoAction::class);
	$app->get('/api/trabajo/borrar', \App\Action\Trabajo\DeleteTrabajoAction::class);
	$app->post('/api/trabajo/actualizar', \App\Action\Trabajo\UpdateTrabajoAction::class);
	$app->post('/api/trabajo/actualizar/app', \App\Action\Trabajo\UpdateTrabajoAppEmpezarAction::class);
	
	/* Tabla Usuarios */
	$app->get('/api/usuario',  \App\Action\Usuario\GetUsuarioAction::class);
	$app->get('/api/usuario/mostrar', \App\Action\Usuario\GetUsuarioByIdAction::class);
	$app->post('/api/usuario/mostrar/app', \App\Action\Usuario\GetUsuarioByEmailAppAction::class);
	$app->post('/api/usuario/borrar', \App\Action\Usuario\DeleteUsuarioAction::class);
	$app->post('/api/usuario/insertar', \App\Action\Usuario\AddUsuarioAction::class);
	$app->get('/api/usuario/login', \App\Action\Usuario\GetUsuarioIsLoginAction::class);
	$app->post('/api/usuario/actualizar', \App\Action\Usuario\UpdateUsuarioAction::class);
	$app->post('/api/usuario/actualizar/app', \App\Action\Usuario\UpdateUsuarioAppAction::class);

};


