<?php

namespace App\Action\Usuario;

use App\Domain\Usuario\Data\Usuario;
use App\Domain\Usuario\Service\UsuarioService;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetUsuarioIsLoginAction
{
    private $usuario;

  
    public function __construct(UsuarioService $usuario)
    {
        $this->usuario = $usuario;
    }

    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {

	   $data = (array)$request->getParsedBody();
		
       $email = (string)($data['email'] ?? '');
       $pass = (string)($data['pass'] ?? '');

       $authorization = explode(' ', (string)$request->getHeaderLine('Authorization'));
		$token = $authorization[1] ?? '';

		if(!$token || !$this->jwtAuth->validateToken($token)){
			$response->getBody()->write((string)json_encode(['status'=>'unsucess']));
			return $response->withHeader('Content-Type', 'application/json')->withStatus(401);
		}

       // Invoke the Domain with inputs and retain the result
       $usuarioData = $this->usuario->getUsuarioIsLogin($email, $pass);
       
       // Build the HTTP response
       $response->getBody()->write((string)json_encode($usuarioData));
       return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
      
    }
}