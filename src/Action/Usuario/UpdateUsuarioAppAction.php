<?php

namespace App\Action\Usuario;

use App\Domain\Usuario\Data\Usuario;
use App\Domain\Usuario\Service\UsuarioService;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class UpdateUsuarioAppAction
{
    private $usuarioService;

  
      public function __construct(UsuarioService $usuarioService)
      {
         $this->usuarioService = $usuarioService;       
      }

    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        
        $data = (array)$request->getParsedBody();
		
        $email = (string)($data['email'] ?? '');
		$pass = (string)($data['pass'] ?? '');		

        $usuarioData = $this->usuarioService->updateUsuarioApp($email,$pass);
       
        // Build the HTTP response
        $response->getBody()->write((string)json_encode($usuarioData));
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
    }
}