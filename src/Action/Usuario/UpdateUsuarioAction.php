<?php

namespace App\Action\Usuario;

use App\Domain\Usuario\Data\Usuario;
use App\Domain\Usuario\Service\UsuarioService;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class UpdateUsuarioAction
{
    private $usuarioService;

  
      public function __construct(UsuarioService $usuarioService)
      {
         $this->usuarioService = $usuarioService;       
      }

    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        
        $data = (array)$request->getParsedBody();
		
        $dni = (string)($data['dni'] ?? '');
		$nombre = (string)($data['nombre_usuario'] ?? '');
		$apellidos = (string)($data['apellidos_usuario'] ?? '');
		$email = (string)($data['email'] ?? '');
		$contrasena = (string)($data['contraseña'] ?? '');
		$role = (string)($data['role'] ?? '');
		
        // Mapping (should be done in a mapper class)
        $usuario = new Usuario();
        $usuario->dni = $dni;
        $usuario->nombre_usuario = $nombre;
        $usuario->apellidos_usuario = $apellidos;
        $usuario->email = $email;
        $usuario->contraseña = $contrasena;
        $usuario->role = $role;
        
        /*$authorization = explode(' ', (string)$request->getHeaderLine('Authorization'));
		$token = $authorization[1] ?? '';

		if(!$token || !$this->jwtAuth->validateToken($token)){
			$response->getBody()->write((string)json_encode(['status'=>'unsucess']));
			return $response->withHeader('Content-Type', 'application/json')->withStatus(401);
		}*/

       // Invoke the Domain with inputs and retain the result
       $usuarioData = $this->usuarioService->updateUsuario($usuario);
       
       // Build the HTTP response
       $response->getBody()->write((string)json_encode($usuarioData));
       return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
    }
}
