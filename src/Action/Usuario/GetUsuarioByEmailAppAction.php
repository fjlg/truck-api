<?php

namespace App\Action\Usuario;

use App\Domain\Usuario\Data\Usuario;
use App\Domain\Usuario\Service\UsuarioService;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetUsuarioByEmailAppAction
{
    private $usuario;

  
    public function __construct(UsuarioService $usuario)
    {
        $this->usuario = $usuario;
    }

    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {

        $data = (array)$request->getParsedBody();
       // Collect input from the HTTP request
      
       $email = (string)($data['email'] ?? '');


       // Invoke the Domain with inputs and retain the result
       $usuarioData = $this->usuario->getUsuarioByEmailApp($email);
       
       // Build the HTTP response
       $response->getBody()->write((string)json_encode($usuarioData));
       return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
      
    }
}

