<?php

namespace App\Action\Trabajo;

use App\Domain\Trabajo\Data\Trabajo;
use App\Domain\Trabajo\Service\TrabajoService;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetTrabajoAction
{
    private $trabajoService;

  
      public function __construct(TrabajoService $trabajoService)
      {
         $this->trabajoService = $trabajoService;       
      }

    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args = []
    ): ResponseInterface {
        // Invoke the Domain with inputs and retain the result        
        $trabajoData = $this->trabajoService->getTrabajo();        

        // Transform the result into the JSON representation

        // Build the HTTP response
        $response->getBody()->write((string)json_encode($trabajoData));        
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
    }
}




