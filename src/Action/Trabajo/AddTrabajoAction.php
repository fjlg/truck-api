<?php

namespace App\Action\Trabajo;

use App\Domain\Trabajo\Data\Trabajo;
use App\Domain\Trabajo\Service\TrabajoService;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;


final class AddTrabajoAction
{
    private $trabajo;

  
    public function __construct(TrabajoService $trabajo)
    {
        $this->trabajo = $trabajo;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        
        // Collect input from the HTTP request
        $data = (array)$request->getParsedBody();

        // Mapping (should be done in a mapper class)
        $trabajo = new Trabajo();
        $trabajo->id_trabajo = (string)$data['id_trabajo'];
        $trabajo->nombre = (string)$data['nombre'];
        $trabajo->fecha_realizar = (string)$data['fecha_realizar'];
        $trabajo->estado = (string)$data['estado'];
        $trabajo->email = (string)$data['email'];
        $trabajo->nifempresa = (string)$data['nifempresa'];
        $trabajo->precio = (string)$data['precio'];
        $trabajo->peso = (string)$data['peso'];
        $trabajo->lugar_carga = (string)$data['lugar_carga'];
        $trabajo->fecha_inicio = (string)$data['fecha_inicio'];
        $trabajo->fecha_final = (string)$data['fecha_final'];
        $trabajo->ubicacion_inicio = (string)$data['ubicacion_inicio'];
        $trabajo->ubicacion_final = (string)$data['ubicacion_final'];
        $trabajo->nombre_responsable = (string)$data['nombre_responsable'];
        $trabajo->matricula = (string)$data['matricula'];
        $trabajo->observacion = (string)$data['observacion'];
        $trabajo->obra = (string)$data['obra'];

        $authorization = explode(' ', (string)$request->getHeaderLine('Authorization'));
		$token = $authorization[1] ?? '';

		/*if(!$token || !$this->jwtAuth->validateToken($token)){
			$response->getBody()->write((string)json_encode(['status'=>'unsucess']));
			return $response->withHeader('Content-Type', 'application/json')->withStatus(401);
		}*/
        
        // Invoke the Domain with inputs and retain the result
        $result = $this->trabajo->AddTrabajo($trabajo);
        // Build the HTTP response
        $response->getBody()->write((string)json_encode($result));

        return $response->withHeader('Content-Type', 'application/json')->withStatus(201)
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    }
}