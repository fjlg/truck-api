<?php

namespace App\Action\Trabajo;

use App\Domain\Trabajo\Data\Trabajo;
use App\Domain\Trabajo\Service\TrabajoService;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class UpdateTrabajoAppEmpezarAction
{
    private $trabajo;

  
    public function __construct(TrabajoService $trabajo)
    {
        $this->trabajo = $trabajo;
    }

    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
    

       $data = (array)$request->getParsedBody();
       
	   $id_trabajo = (string)($data['id_trabajo'] ?? '');
       $fecha_inicio = (string)($data['fecha_inicio'] ?? '');
       $fecha_final = (string)($data['fecha_final'] ?? '');
       $ubicacion_inicio = (string)($data['ubicacion_inicio'] ?? '');
       $ubicacion_final = (string)($data['ubicacion_final'] ?? '');
       $estado = (string)($data['estado'] ?? '');
       $nombre_responsable = (string)($data['nombre_responsable'] ?? '');
	   
	   if(empty($fecha_final)){
		   $trabajoData = $this->trabajo->updateAppEmpezarTrabajo($id_trabajo, $fecha_inicio, $ubicacion_inicio);
	   }else{
		   $trabajoData = $this->trabajo->updateAppTerminarTrabajo($id_trabajo, $fecha_final, $ubicacion_final, $nombre_responsable);
	   }
       
       // Build the HTTP response
       $response->getBody()->write((string)json_encode($trabajoData));
       return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
    }
}