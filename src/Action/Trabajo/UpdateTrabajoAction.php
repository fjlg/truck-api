<?php

namespace App\Action\Trabajo;

use App\Domain\Trabajo\Data\Trabajo;
use App\Domain\Trabajo\Service\TrabajoService;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class UpdateTrabajoAction
{
    private $trabajo;

  
    public function __construct(TrabajoService $trabajo)
    {
        $this->trabajo = $trabajo;
    }

    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
    

       $data = (array)$request->getParsedBody();

       $id_trabajo = (string)($data['id_trabajo'] ?? '');
       $nombre = (string)($data['nombre'] ?? '');
       $fecha_realizar = (string)($data['fecha_realizar'] ?? '');
       $estado= (string)($data['estado'] ?? '');
       $email = (string)($data['email'] ?? '');
       $nifempresa = (string)($data['nifempresa'] ?? '');
       $precio = (string)($data['precio'] ?? '');
       $peso = (string)($data['peso'] ?? '');
       $fecha_inicio = (string)($data['fecha_inicio'] ?? '');
       $fecha_final = (string)($data['fecha_final'] ?? '');
       $ubicacion_inicio = (string)($data['ubicacion_inicio'] ?? '');
       $ubicacion_final = (string)($data['ubicacion_final'] ?? '');
       $nombre_responsable = (string)($data['nombre_responsable'] ?? '');
       $matricula = (string)($data['matricula'] ?? '');
       $observacion= (string)($data['observacion'] ?? '');
       $obra= (string)($data['obra'] ?? '');

       $trabajo = new Trabajo();
       $trabajo->id_trabajo= $id_trabajo;
       $trabajo->nombre=$nombre;
       $trabajo->fecha_realizar =$fecha_realizar;
       $trabajo->estado =$estado;
       $trabajo->email =$email;
       $trabajo->nifempresa=$nifempresa;
       $trabajo->precio = $precio;
       $trabajo->peso= $peso;
       $trabajo->fecha_inicio= $fecha_inicio;
       $trabajo->fecha_final= $fecha_final;
       $trabajo->ubicacion_inicio= $ubicacion_inicio;
       $trabajo->ubicacion_final= $ubicacion_final;
       $trabajo->nombre_responsable= $nombre_responsable;
       $trabajo->matricula= $matricula;
       $trabajo->observacion= $observacion;
       $trabajo->obra= $obra;

       /*$authorization = explode(' ', (string)$request->getHeaderLine('Authorization'));
		$token = $authorization[1] ?? '';

		if(!$token || !$this->jwtAuth->validateToken($token)){
			$response->getBody()->write((string)json_encode(['status'=>'unsucess']));
			return $response->withHeader('Content-Type', 'application/json')->withStatus(401);
		}*/

       // Invoke the Domain with inputs and retain the result
       $trabajoData = $this->trabajo->updateTrabajo($trabajo);
       
       // Build the HTTP response
       $response->getBody()->write((string)json_encode($trabajoData));
       return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
    }
}




