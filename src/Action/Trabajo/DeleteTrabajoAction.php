<?php

namespace App\Action\Trabajo;

use App\Domain\Trabajo\Data\Trabajo;
use App\Domain\Trabajo\Service\TrabajoService;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class DeleteTrabajoAction
{
    private $trabajo;

  
    public function __construct(TrabajoService $trabajo)
    {
        $this->trabajo = $trabajo;
    }

    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        // Invoke the Domain with inputs and retain the result        
       // Collect input from the HTTP request
       $data = (array)$request->getParsedBody();

       $id = (string)($data['id_trabajo'] ?? '');

       $authorization = explode(' ', (string)$request->getHeaderLine('Authorization'));
		$token = $authorization[1] ?? '';

		if(!$token || !$this->jwtAuth->validateToken($token)){
			$response->getBody()->write((string)json_encode(['status'=>'unsucess']));
			return $response->withHeader('Content-Type', 'application/json')->withStatus(401);
		}

       // Invoke the Domain with inputs and retain the result
       $trabajoData = $this->trabajo->deleteTrabajo($id);
       
       // Build the HTTP response
       $response->getBody()->write((string)json_encode($trabajoData));
       return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
    }
}




