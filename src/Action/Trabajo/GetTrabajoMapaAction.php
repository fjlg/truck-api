<?php

namespace App\Action\Trabajo;

use App\Domain\Trabajo\Data\Trabajo;
use App\Domain\Trabajo\Service\TrabajoService;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetTrabajoMapaAction
{
    private $trabajoService;

  
      public function __construct(TrabajoService $trabajoService)
      {
         $this->trabajoService = $trabajoService;       
      }

    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args = []
    ): ResponseInterface {
        
		$data = (array)$request->getParsedBody();
		
        $fechadia = (string)($data['fechadia'] ?? '');
    
        $trabajoData = $this->trabajoService->getTrabajoMapa($fechadia);        

        // Transform the result into the JSON representation

        // Build the HTTP response
        $response->getBody()->write((string)json_encode($trabajoData));        
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
    }
}
