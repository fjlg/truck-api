<?php

namespace App\Action;

use App\Auth\JwtAuth;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use PDO;
use App\Domain\Usuario\Data\Usuario;

final class TokenCreateAction
{
    private $jwtAuth;
	private $connection;

    public function __construct(JwtAuth $jwtAuth, PDO $connection)
    {
        $this->jwtAuth = $jwtAuth;
		$this->connection = $connection;
    }
	
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
		$data = (array)$request->getParsedBody();
		
        $email = (string)($data['email'] ?? '');
        $pass = (string)($data['pass'] ?? '');
		
		$sql = "SELECT `role` FROM usuarios WHERE email='$email' AND contraseña=MD5('$pass')";
        $statement = $this->connection->prepare($sql);
        $statement->execute();

		$row = $statement->fetch();
		
        if (empty($row)) {
			$response->getBody()->write((string)json_encode(['status' => 'unsuccess','access_token' => '','token_type' => 'Bearer','expires_in' => '','role' => '']));   
			return $response->withHeader('Content-Type', 'application/json')
                ->withStatus(201, 'Unauthorized');
        } 
        
        // Create a fresh token
        $token = $this->jwtAuth->createJwt($email, $row['role']);        
        $lifetime = $this->jwtAuth->getLifetime();

        $result = [
			'status' => 'success',
            'access_token' => $token,
            'token_type' => 'Bearer',
            'expires_in' => $lifetime,
			'role' => $row['role'],
        ];

        $response = $response->withHeader('Content-Type', 'application/json');
        $response->getBody()->write((string)json_encode($result));
        return $response->withStatus(201);
    }
}