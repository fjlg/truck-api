<?php

namespace App\Action\Empresa;

use App\Domain\Empresa\Data\Empresa;
use App\Domain\Empresa\Service\EmpresaService;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class AddEmpresaAction
{
    private $empresaService;

    public function __construct(EmpresaService $empresaService)
    {
        $this->empresaService = $empresaService;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        // Collect input from the HTTP request
        $data = (array)$request->getParsedBody();

        // Mapping (should be done in a mapper class)
        $empresa = new Empresa();
        $empresa->nif_empresa = (string)$data['nif_empresa'];
        $empresa->nombre_empresa = (string)$data['nombre_empresa'];
        $empresa->direccion = (string)$data['direccion'];
        $empresa->provincia = (string)$data['provincia'];
        $empresa->poblacion = (string)$data['poblacion'];
        $empresa->codigo_postal = (string)$data['codigo_postal'];
        $empresa->telefono = (string)$data['telefono'];
        $empresa->email_empresa = (string)$data['email_empresa'];

        $authorization = explode(' ', (string)$request->getHeaderLine('Authorization'));
		$token = $authorization[1] ?? '';

		/*if(!$token || !$this->jwtAuth->validateToken($token)){
			$response->getBody()->write((string)json_encode(['status'=>'unsucess']));
			return $response->withHeader('Content-Type', 'application/json')->withStatus(401);
		}*/

        // Invoke the Domain with inputs and retain the result
        $result = $this->empresaService->AddEmpresa($empresa);
        
        // Build the HTTP response
        $response->getBody()->write((string)json_encode($result));

        return $response->withHeader('Content-Type', 'application/json')->withStatus(201);
    }
}