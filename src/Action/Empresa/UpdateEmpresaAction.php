<?php

namespace App\Action\Empresa;

use App\Domain\Empresa\Data\Empresa;
use App\Domain\Empresa\Service\EmpresaService;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class UpdateEmpresaAction
{
    private $empresaService;

  
    public function __construct(EmpresaService $empresaService)
    {
       $this->empresaService = $empresaService;       
    }


    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        
        $data = (array)$request->getParsedBody();
		
        $nif_empresa = (string)($data['nif_empresa'] ?? '');
		$nombre = (string)($data['nombre_empresa'] ?? '');
		$direccion = (string)($data['direccion'] ?? '');
		$provincia= (string)($data['provincia'] ?? '');
        $poblacion = (string)($data['poblacion'] ?? '');
        $codigopostal = (string)($data['codigo_postal'] ?? '');
        $telefono = (string)($data['telefono'] ?? '');
        $email = (string)($data['email_empresa'] ?? '');
        
        // Mapping (should be done in a mapper class)
        $empresa = new Empresa();
        $empresa->nif_empresa = $nif_empresa;
        $empresa->nombre_empresa=$nombre;
        $empresa->direccion =$direccion;
        $empresa->provincia =$provincia;
        $empresa->poblacion =$poblacion;
        $empresa->codigo_postal=$codigopostal;
        $empresa->telefono = $telefono;
        $empresa->email_empresa= $email;
            
        /*$authorization = explode(' ', (string)$request->getHeaderLine('Authorization'));
		$token = $authorization[1] ?? '';

		if(!$token || !$this->jwtAuth->validateToken($token)){
			$response->getBody()->write((string)json_encode(['status'=>'unsucess']));
			return $response->withHeader('Content-Type', 'application/json')->withStatus(401);
		}*/

       // Invoke the Domain with inputs and retain the result
       $empresaData = $this->empresaService->updateEmpresa($empresa);
       
       // Build the HTTP response
       $response->getBody()->write((string)json_encode($empresaData));
       return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
    }
}
