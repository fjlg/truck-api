<?php

namespace App\Action\Empresa;

use App\Domain\Empresa\Data\Empresa;
use App\Domain\Empresa\Service\EmpresaService;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetEmpresaByIdAction
{
    private $empresa;

  
    public function __construct(EmpresaService $empresa)
    {
        $this->empresa = $empresa;
    }

    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {

       $data = (array)$request->getParsedBody();

       $nif_empresa = (string)($data['nif_empresa'] ?? '');


       // Invoke the Domain with inputs and retain the result
       $empresaData = $this->empresa->getEmpresaById($nif_empresa);
       
       // Build the HTTP response
       $response->getBody()->write((string)json_encode($empresaData));
       return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
      
    }
}








