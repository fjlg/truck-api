<?php

namespace App\Action\Empresa;

use App\Domain\Empresa\Data\Empresa;
use App\Domain\Empresa\Service\EmpresaService;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Auth\JwtAuth;

final class GetEmpresaAction
{
    private $empresaService;
	private $jwtAuth;

    public function __construct(JwtAuth $jwtAuth, EmpresaService $empresaService)
    {
        $this->empresaService = $empresaService;  
	    $this->jwtAuth = $jwtAuth;
    }

    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args = []
    ): ResponseInterface {

		$authorization = explode(' ', (string)$request->getHeaderLine('Authorization'));
		$token = $authorization[1] ?? '';

		/*if(!$token || !$this->jwtAuth->validateToken($token)){
			$response->getBody()->write((string)json_encode(['status'=>'unsucess']));
			return $response->withHeader('Content-Type', 'application/json')->withStatus(401);
		}
		else{*/
			
			$empresaData = $this->empresaService->getEmpresa();        

			$response->getBody()->write((string)json_encode($empresaData));        
			return $response->withHeader('Access-Control-Allow-Origin', '*')
			->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
			->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS')
			->withHeader('Content-Type', 'application/json')->withStatus(200);
		}
    }
//}




