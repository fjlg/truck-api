<?php

namespace App\Action\Empresa;

use App\Domain\Empresa\Data\Empresa;
use App\Domain\Empresa\Service\EmpresaService;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class DeleteEmpresaAction
{
    private $empresaService;

  
      public function __construct(EmpresaService $empresaService)
      {
         $this->empresaService = $empresaService;       
      }

    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        // Invoke the Domain with inputs and retain the result        
       // Collect input from the HTTP request
       $data = (array)$request->getParsedBody();
		
       $nif_empresa = (string)($data['nif_empresa'] ?? '');

       $authorization = explode(' ', (string)$request->getHeaderLine('Authorization'));
		$token = $authorization[1] ?? '';

		if(!$token || !$this->jwtAuth->validateToken($token)){
			$response->getBody()->write((string)json_encode(['status'=>'unsucess']));
			return $response->withHeader('Content-Type', 'application/json')->withStatus(401);
		}

       // Invoke the Domain with inputs and retain the result
       $empresaData = $this->empresaService->deleteEmpresa($nif_empresa);
       
       // Build the HTTP response
       $response->getBody()->write((string)json_encode($empresaData));
       return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
    }
}




