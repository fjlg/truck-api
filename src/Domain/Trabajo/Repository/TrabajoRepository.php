<?php

namespace App\Domain\Trabajo\Repository;
use App\Domain\Trabajo\Data\Trabajo;
use PDO;


/**
 * Repository.
 */
class TrabajoRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Insert user row.
     *
     * @param Trabajo $user The user
     *
     * @return int The new ID
     */
    public function AddTrabajo(Trabajo $trabajo):string
    {
        $id_trabajo = $trabajo->id_trabajo;
        $nombre = $trabajo->nombre;
        $fecha_realizar = $trabajo->fecha_realizar;
        $estado = $trabajo->estado;
        $email = $trabajo->email;
        $nifempresa = $trabajo->nifempresa;
        $precio =$trabajo->precio;
        $peso= $trabajo->peso;
        $lugar_carga= $trabajo->lugar_carga;
        $fecha_inicio= $trabajo->fecha_inicio;
        $fecha_final= $trabajo->fecha_final;
        $ubicacion_inicio= $trabajo->ubicacion_inicio;
        $ubicacion_final= $trabajo->ubicacion_final;
        $nombre_responsable= $trabajo->nombre_responsable;
        $matricula= $trabajo->matricula;
        $observacion= $trabajo->observacion;
        $obra= $trabajo->obra;

        $sql = "INSERT INTO trabajos SET 
        id_trabajo='". $id_trabajo ."', 
        nombre='".$nombre."', 
        fecha_realizar='".$fecha_realizar."', 
        estado='".$estado."', 
        email='".$email."',
        nifempresa='".$nifempresa."',
        precio='".$precio."',
        peso='".$peso."',
        lugar_carga='".$lugar_carga."',
        fecha_inicio='".$fecha_inicio."',
        ubicacion_inicio='".$ubicacion_inicio."',
        ubicacion_final='".$ubicacion_final."',
        nombre_responsable='".$nombre_responsable."',
        matricula='".$matricula."',
        observacion='".$observacion."',
        obra='".$obra."'";

        $this->connection->prepare($sql)->execute();

        return "Se ha añadido el Trabajo correctamente.";
    }

    public function deleteTrabajo(string $trabajoId)
    {
   
        $query = 'DELETE FROM `trabajos` WHERE `id_trabajo` = :id';
        $query2= 'SELECT id_trabajo FROM `trabajos` WHERE `id_trabajo` = :id';
    
        $statement2 = $this->connection->prepare($query2);
        $statement2->bindParam('id', $trabajoId);
        $statement2->execute();
        $trabajo=$statement2->fetchAll();  
        
        if(empty($trabajo)){

            return 'El trabajo no existe' ;
        }
        else{

            $statement = $this->connection->prepare($query);
            $statement->bindParam('id', $trabajoId);
            $statement->execute();
            return 'El Trabajo ha sido borrado.';
        }
    }

    public function updateTrabajo(Trabajo $trabajo):string
    {
        $id_trabajo=$trabajo->id_trabajo;
        $nombre=$trabajo->nombre;
        $fecha_realizar=$trabajo->fecha_realizar;
        $estado= $trabajo->estado;
        $email=$trabajo->email;
        $nifempresa=$trabajo->nifempresa;
        $precio=$trabajo->precio;
        $peso=$trabajo->peso;
        $fecha_inicio=$trabajo->fecha_inicio;
        $fecha_final=$trabajo->fecha_final;
        $ubicacion_inicio=$trabajo->ubicacion_inicio;
        $ubicacion_final=$trabajo->ubicacion_final;
        $nombre_responsable=$trabajo->nombre_responsable;
        $matricula=$trabajo->matricula;
        $observacion=$trabajo->observacion;
        $obra= $trabajo->obra;


        $query ="UPDATE trabajos SET `id_trabajo`='". $id_trabajo."',
        `nombre`='".$nombre."',`fecha_realizar`='".$fecha_realizar."',
        `estado`='".$estado."', `email`='".$email."',
        `nifempresa`='".$nifempresa."',`precio`='".$precio."',
        `peso`='".$peso."',`fecha_inicio`='".$fecha_inicio."',
        `fecha_final`='".$fecha_final."',`ubicacion_inicio`='".$ubicacion_inicio."',
        `ubicacion_final`='".$ubicacion_final."',`nombre_responsable`='".$nombre_responsable."',
        `matricula`='".$matricula."',`observacion`='".$observacion."',`obra`='".$obra."'WHERE `id_trabajo` = :id;";
        
        
        $query2= 'SELECT id_trabajo FROM `trabajos` WHERE `id_trabajo` = :id';
        
        $statement2 = $this->connection->prepare($query2);
        $statement2->bindParam('id', $id_trabajo);
        $statement2->execute();
        $trabajo=$statement2->fetchAll();  
    
        if(empty($trabajo)){
    
            return 'El trabajo no existe' ;
        }
    
        else {
        $statement = $this->connection->prepare($query);
        $statement->bindParam('id', $id_trabajo);
        $statement->execute();
    
        return 'Se han actualizado los Trabajos';
    
        }
    }

    public function getTrabajo(): array
    {                
        $sql = "SELECT * FROM trabajos INNER JOIN empresas ON trabajos.nifempresa = empresas.nif_empresa ORDER BY `fecha_realizar` DESC,`id_trabajo`";            
        $statement = $this->connection->prepare($sql);
        
        $statement->execute();
        
        $trabajos = $statement->fetchAll();        

        if (empty($trabajos)) {
            throw new DomainException('No existe ningun registro de trabajos.', 404);
        }
        return $trabajos ;

    }

    public function getTrabajoByEmail(string $email):array
    {
        $sql = "SELECT * FROM `trabajos` WHERE email =:email ORDER BY fecha_realizar DESC";
        $statement = $this->connection->prepare($sql);
        $statement->execute(['email' => $email]);

        $trabajos = $statement->fetchAll();

        return $trabajos;
    }
	
	public function getTrabajoById(string $trabajoId):Trabajo
    {
        $sql = "SELECT * FROM `trabajos` WHERE id_trabajo = :id;";
        $statement = $this->connection->prepare($sql);
        $statement->execute(['id' => $trabajoId]);

        $row = $statement->fetch();

        if (empty($row)) {

            $trabajo = new Trabajo();
            $trabajo->id_trabajo = null;
            $trabajo->nombre = null;
            $trabajo->fecha_realizar = null;
            $trabajo->estado = null;
            $trabajo->email = null;
            $trabajo->nifempresa = null;
            $trabajo->precio = null;
            $trabajo->peso = null;
            $trabajo->fecha_inicio = null;
            $trabajo->fecha_final = null;
            $trabajo->ubicacion_inicio = null;
            $trabajo->ubicacion_final = null;
            $trabajo->nombre_responsable = null;
            $trabajo->matricula = null;
            $trabajo->observacion = null;
            $trabajo->obra = null;
            return $trabajo;
        }

        // Map array to data object
        $trabajo = new Trabajo();
        $trabajo->id_trabajo = (int)$row['id_trabajo'];
        $trabajo->nombre = (string)$row['nombre'];
        $trabajo->fecha_realizar = (string)$row['fecha_realizar'];
        $trabajo->estado = (string)$row['estado'];
        $trabajo->email = (string)$row['email'];
        $trabajo->nifempresa = (string)$row['nifempresa'];
        $trabajo->precio = (string)$row['precio'];
        $trabajo->peso = (string)$row['peso'];
        $trabajo->fecha_inicio = (string)$row['fecha_inicio'];
        $trabajo->fecha_final = (string)$row['fecha_final'];
        $trabajo->ubicacion_inicio = (string)$row['ubicacion_inicio'];
        $trabajo->ubicacion_final = (string)$row['ubicacion_final'];
        $trabajo->nombre_responsable = (string)$row['nombre_responsable'];
        $trabajo->matricula = (string)$row['matricula'];
        $trabajo->observacion = (string)$row['observacion'];
        $trabajo->obra = (string)$row['obra'];

        return $trabajo;
    }
	
	public function updateAppEmpezarTrabajo(string $id_trabajo, string $fecha_inicio, string $ubicacion_inicio):string
    {
        $query ="UPDATE trabajos SET `estado`=2,`fecha_inicio`='".$fecha_inicio."',`ubicacion_inicio`='".$ubicacion_inicio."' WHERE `id_trabajo` = :id;";
        
        $statement = $this->connection->prepare($query);
        $statement->bindParam('id', $id_trabajo);
        $statement->execute();
    
        return 'Se han actualizado el Trabajo';
    
    }
	
	public function updateAppTerminarTrabajo(string $id_trabajo, string $fecha_final, string $ubicacion_final, string $nombre_responsable):string
    {
        $query ="UPDATE trabajos SET `estado`=1, `nombre_responsable`='".$nombre_responsable."',`fecha_final`='".$fecha_final."',`ubicacion_final`='".$ubicacion_final."' WHERE `id_trabajo` = :id;";
        
        $statement = $this->connection->prepare($query);
        $statement->bindParam('id', $id_trabajo);
        $statement->execute();
    
        return 'Se han actualizado el Trabajo';
    
    }
	
	public function getTrabajoMapa(string $fechadia): array
    {                
        $sql = "SELECT * FROM trabajos INNER JOIN empresas ON trabajos.nifempresa = empresas.nif_empresa WHERE `fecha_realizar`='".$fechadia."' ORDER BY `fecha_realizar` DESC,`id_trabajo`";            
        $statement = $this->connection->prepare($sql);
        
        $statement->execute();
        
        $trabajos = $statement->fetchAll();        

        if (empty($trabajos)) {
            throw new DomainException('No existe ningun registro de trabajos.', 404);
        }
        return $trabajos ;

    }
}