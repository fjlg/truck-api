<?php

namespace App\Domain\Trabajo\Service;

use App\Domain\Trabajo\Data\Trabajo;
use App\Domain\Trabajo\Repository\TrabajoRepository;
use InvalidArgumentException;

/**
 * Service.
 */
final class TrabajoService
{
    /**
     * @var TrabajoRepository
     */
    private $repository;

    /**
     * The constructor.
     *
     * @param TrabajoRepository $repository The repository
     */
    public function __construct(TrabajoRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Create a new user.
     *
     * @param Trabajo $user The user data
     *
     * @throws InvalidArgumentException
     *
     * @return int The new user ID
     */
    public function AddTrabajo(Trabajo $trabajo):string
    {
        // Validation
        if (empty($trabajo->id_trabajo)) {
            throw new InvalidArgumentException('ID Trabajo requerida');
        }

        // Insert user
        $trabajoId = $this->repository->AddTrabajo($trabajo);

        // Logging here: Trabajo created successfully

        return $trabajoId;
    }


    public function getTrabajo():array
    {
        //     return $result;
        $trabajos = $this->repository->getTrabajo();
        return $trabajos;
    }


    public function getTrabajoById(string $id_trabajo):Trabajo
    {
        if(empty($id_trabajo)){
            throw new InvalidArgumentException('ID Trabajo requerida');
        }

        $trabajo = $this->repository->getTrabajoById($id_trabajo);
        return $trabajo;
    }

	public function getTrabajoByEmail(string $email):array
    {
        if(empty($email)){
            throw new InvalidArgumentException('ID Trabajo requerida');
        }

        $trabajo = $this->repository->getTrabajoByEmail($email);
        return $trabajo;
    }

    public function deleteTrabajo(string $id_trabajo):string
    {
        if(empty($id_trabajo)){
            throw new InvalidArgumentException('ID Trabajo requerida.');
        }

        $result = $this->repository->deleteTrabajo($id_trabajo);
        return $result;
    }   


    public function updateTrabajo(Trabajo $trabajo):string
    {
        if(empty($trabajo)){
            throw new InvalidArgumentException(' Trabajo Requerido.');
        }

        $result = $this->repository->updateTrabajo($trabajo);
        return $result;
    } 
	
	public function updateAppEmpezarTrabajo(string $id_trabajo, string $fecha_inicio, string $ubicacion_inicio):string
    {
        $result = $this->repository->updateAppEmpezarTrabajo($id_trabajo, $fecha_inicio, $ubicacion_inicio);
        return $result;
    } 
	
	public function updateAppTerminarTrabajo(string $id_trabajo, string $fecha_final, string $ubicacion_final, string $nombre_responsable):string
    {
        $result = $this->repository->updateAppTerminarTrabajo($id_trabajo, $fecha_final, $ubicacion_final, $nombre_responsable);
        return $result;
    } 
	
	
	public function getTrabajoMapa(string $fechadia):array
    {
        //     return $result;
        $trabajos = $this->repository->getTrabajoMapa($fechadia);
        return $trabajos;
    }

}