<?php

namespace App\Domain\Trabajo\Data;

final class Trabajo
{
    /** @var int */
    public $id_trabajo;
    
    /** @var string */
    public $nombre;

    /** @var string */
    public $fecha_realizar;

    /** @var string */
    public $estado;

    /** @var string */
    public $email;
    
    /** @var string */
    public $nifempresa;
    
    /** @var string */
    public $precio;
    
    /** @var string */
    public $peso;

     /** @var string */
     public $lugar_carga;
    
    /** @var string */
    public $fecha_inicio;
    
    /** @var string */
    public $fecha_final;
    
    /** @var string */
    public $ubicacion_inicio;
    
    /** @var string */
    public $ubicacion_final;
    
    /** @var string */
    public $nombre_responsable;
    
    /** @var string */
    public $matricula;
    
    /** @var string */
    public $observacion;
    
    /** @var string */
    public $obra;
    
}