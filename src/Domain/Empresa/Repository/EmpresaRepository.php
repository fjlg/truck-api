<?php

namespace App\Domain\Empresa\Repository;
use App\Domain\Empresa\Data\Empresa;
use PDO;


/**
 * Repository.
 */
class EmpresaRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Insert empresa row.
     *
     * @param Empresa $empresa The empresa
     *
     * @return int The new ID
     */
    public function AddEmpresa(Empresa $empresa): string
    {
 
        $nif_empresa = $empresa->nif_empresa;
        $nombre = $empresa->nombre_empresa;
        $direccion = $empresa->direccion;
        $provincia = $empresa->provincia;
        $poblacion = $empresa->poblacion;
        $codigopostal = $empresa->codigo_postal;
        $telefono =$empresa->telefono;
        $email= $empresa->email_empresa;

$sql = "INSERT INTO empresas SET 
        nif_empresa='". $nif_empresa ."', 
        nombre_empresa='".$nombre."', 
        direccion='".$direccion."', 
        provincia='".$provincia."', 
        poblacion='".$poblacion."',
        codigo_postal='".$codigopostal."',
        telefono='".$telefono."',
        email_empresa='".$email."'";

        $this->connection->prepare($sql)->execute();

        return 'se ha añadido correctamente.';
    }

    public function deleteEmpresa(string $nif_empresa)
    {
        $query = 'DELETE FROM `empresas` WHERE `nif_empresa` = :id';
        $query2= 'SELECT nif_empresa FROM `empresas` WHERE `nif_empresa` = :id';
    
        $statement2 = $this->connection->prepare($query2);
        $statement2->bindParam('id', $nif_empresa);
        $statement2->execute();
        $empresa=$statement2->fetchAll();  
        
        if(empty($empresa)){

            return 'La empresa no existe' ;
        }
        else{

            $statement = $this->connection->prepare($query);
            $statement->bindParam('id', $nif_empresa);
            $statement->execute();
            return 'La empresa ha sido borrada.';
        }
      
    }


    public function updateEmpresa(Empresa $empresa):string
    {

        $nif_empresa = $empresa->nif_empresa;
        $nombre= $empresa->nombre_empresa;
        $direccion= $empresa->direccion;
        $provincia= $empresa->provincia;
        $poblacion = $empresa->poblacion;
        $codigopostal = $empresa->codigo_postal;
        $telefono = $empresa->telefono;
        $email = $empresa->email_empresa;
            
    
        $query ="UPDATE empresas SET `nombre_empresa`='".$nombre."',
        `direccion`='".$direccion."',`provincia`='".$provincia."',
        `poblacion`='".$poblacion."', `codigo_postal`='".$codigopostal."',
        `telefono`='".$telefono."',`email_empresa`='".$email."' WHERE `nif_empresa` = :id;";
        
        
        $query2= 'SELECT nif_empresa FROM `empresas` WHERE `nif_empresa` = :id';
        
        $statement2 = $this->connection->prepare($query2);
        $statement2->bindParam('id', $nif_empresa);
        $statement2->execute();
        $empresa=$statement2->fetchAll();  
    
        if(empty($empresa)){
    
            return 'La empresa no existe' ;
        }
    
        else {
        $statement = $this->connection->prepare($query);
        $statement->bindParam('id', $nif_empresa);
        $statement->execute();
    
        return 'Se han actualizado los datos';
    
        }

    }

    public function getEmpresa(): array
    {                
        $sql = "SELECT `nif_empresa`, `nombre_empresa`, `direccion`, `provincia`, `poblacion`, `codigo_postal`, `telefono`, `email_empresa` FROM empresas";            
        $statement = $this->connection->prepare($sql);
        
        $statement->execute();
        
        $empresas = $statement->fetchAll();        

        if (empty($empresas)) {
            throw new DomainException('No existe ningun registro de empresas.', 404);
        }
        return $empresas;

    }

    public function getEmpresaById(string $nif_empresa):array
    {
        $sql = "SELECT * FROM empresas WHERE nif_empresa = :id;";
        $statement = $this->connection->prepare($sql);
        $statement->execute(['id' => $nif_empresa]);

        $empresa = $statement->fetchAll();

       
        return $empresa;
    }






}

