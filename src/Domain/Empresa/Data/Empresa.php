<?php

namespace App\Domain\Empresa\Data;

final class Empresa
{
    /** @var string */
    public $nif_empresa;
    
    /** @var string */
    public $nombre_empresa;

    /** @var string */
    public $direccion;

    /** @var string */
    public $provincia;

    /** @var string */
    public $poblacion;
    
    /** @var string */
    public $codigo_postal;
    
    /** @var string */
    public $telefono;
    
    /** @var string */
    public $email_empresa;
}

