<?php

namespace App\Domain\Empresa\Service;

use App\Domain\Empresa\Data\Empresa;
use App\Domain\Empresa\Repository\EmpresaRepository;
use InvalidArgumentException;

/**
 * Service.
 */
final class EmpresaService
{
    /**
     * @var EmpresaRepository
     */
    private $repository;

    /**
     * The constructor.
     *
     * @param EmpresaRepository $repository The repository
     */
    public function __construct(EmpresaRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Create a new empresa.
     *
     * @param Empresa $empresa The empresa data
     *
     * @throws InvalidArgumentException
     *
     * @return int The new empresa ID
     */
    public function AddEmpresa(Empresa $empresa): string
    {
        // Validation
        if (empty($empresa->nif_empresa)) {
            throw new InvalidArgumentException('CIF/NIF Empresa requerido.');
        }

        // Insert empresa
        $nif_empresa = $this->repository->AddEmpresa($empresa);

        // Logging here: Empresa created successfully

        return $nif_empresa;
    }


    public function getEmpresa():array
    {
        //     return $result;
        $empresas = $this->repository->getEmpresa();
        return $empresas;
    }


    public function getEmpresaById(string $nif_empresa):array
    {
        if(empty($nif_empresa)){
            throw new InvalidArgumentException('Empresa ID required');
        }

        $empresa = $this->repository->getEmpresaById($nif_empresa);
        return $empresa;
    }


    public function deleteEmpresa(string $nif_empresa):string
    {
        if(empty($nif_empresa)){
            throw new InvalidArgumentException('Empresa NIF/CIF requerido.');
        }

        $result = $this->repository->deleteEmpresa($nif_empresa);
        return $result;
    }   

    public function updateEmpresa(Empresa $empresa):string
    {
        if(empty($empresa)){
            throw new InvalidArgumentException(' Empresa requerida.');
        }

        $result = $this->repository->updateEmpresa($empresa);
        return $result;
    } 
}
