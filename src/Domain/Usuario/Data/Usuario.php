<?php

namespace App\Domain\Usuario\Data;

final class Usuario
{
    /** @var string */  
    public $dni;
    
    /** @var string */
    public $nombre_usuario;

    /** @var string */
    public $apellidos_usuario;

    /** @var string */
    public $email;

    /** @var string */
    public $contraseña;
	
	/** @var string */
    public $role;
}