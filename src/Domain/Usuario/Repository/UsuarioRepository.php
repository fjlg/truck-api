<?php

namespace App\Domain\Usuario\Repository;
use App\Domain\Usuario\Data\Usuario;
use PDO;


/**
 * Repository.
 */
class UsuarioRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Insert user row.
     *
     * @param Usuario $user The user
     *
     * @return int The new ID
     */
    public function insertUsuario(Usuario $usuario):string
    {
        $dni = $usuario->dni;
        $nombre = $usuario->nombre_usuario;
		$apellidos = $usuario->apellidos_usuario;
		$email = $usuario->email;
		$contrasena = $usuario->contraseña;
		$role = $usuario->role;
        
        $contrasena=MD5($contrasena);

        $sql = "INSERT INTO usuarios SET 
                dni='".$dni."', 
                nombre_usuario='".$nombre."', 
                apellidos_usuario='".$apellidos."', 
                email='".$email."', 
                contraseña='".$contrasena."',
				role='".$role."'";

        $this->connection->prepare($sql)->execute();
		
        return "Se ha añadido el Usuario correctamente.";
    }

    public function deleteUsuario(string $usuarioId)
    {
        $query = 'DELETE FROM `usuarios` WHERE `dni` = :id';
        $query2= 'SELECT dni FROM `usuarios` WHERE `dni` = :id';
    
        $statement2 = $this->connection->prepare($query2);
        $statement2->bindParam('id', $usuarioId);
        $statement2->execute();
        $usuarios=$statement2->fetchAll();  
        
        if(empty($usuarios)){

            return 'El usuario no existe' ;
        }
        else{

            $statement = $this->connection->prepare($query);
            $statement->bindParam('id', $usuarioId);
            $statement->execute();
            return 'El usuario ha sido borrado.';
        }
      
    }

    public function updateUsuario(Usuario $usuario):string
    {
    $dni = $usuario->dni;
    $nombre = $usuario->nombre_usuario;
	$apellidos = $usuario->apellidos_usuario;
	$email = $usuario->email;
    $contrasena = $usuario->contraseña;
	$role = $usuario->role;
        

    $contrasena=MD5($contrasena);
    
    $query ="UPDATE usuarios SET `nombre_usuario`='".$nombre."',
    `apellidos_usuario`='".$apellidos."',`email`='".$email."',
    `contraseña`='".$contrasena."',`role`='".$role."' WHERE `dni` = :id;";
    
    
    $query2= 'SELECT dni FROM `usuarios` WHERE `dni` = :id';
    
    $statement2 = $this->connection->prepare($query2);
    $statement2->bindParam('id', $dni);
    $statement2->execute();
    $usuarios=$statement2->fetchAll();  

    if(empty($usuarios)){

        return 'El usuario no existe' ;
    }

    else {
    $statement = $this->connection->prepare($query);
    $statement->bindParam('id', $dni);
    $statement->execute();

    return 'Se han actualizado los datos';

    }

    }

    public function getUsuario(): array
    {                
        $sql = "SELECT * FROM usuarios";            
        $statement = $this->connection->prepare($sql);
        
        $statement->execute();
        
        $usuarios = $statement->fetchAll();        

        if (empty($usuarios)) {
            throw new DomainException('No existe ningun registro de usuarios.', 404);
        }
        return $usuarios;

    }

    public function getUsuarioById(string $usuarioId):Usuario
    {
        $sql = "SELECT * from usuarios WHERE dni = :id;";
        $statement = $this->connection->prepare($sql);
        $statement->execute(['id' => $usuarioId]);
        
        
        $row = $statement->fetch();

        if (empty($row)) {
            $usuario = new Usuario();
            $usuario->dni = null;
            $usuario->nombre_usuario = null;
            $usuario->apellidos_usuario = null;
            $usuario->email = null;
            $usuario->contraseña= null;
			$usuario->role= null;
            return $usuario;
        }


        // Map array to data object
        $usuario = new Usuario();
        $usuario->dni = (string)$row['dni'];
        $usuario->nombre_usuario = (string)$row['nombre_usuario'];
        $usuario->apellidos_usuario = (string)$row['apellidos_usuario'];
        $usuario->email = (string)$row['email'];
        $usuario->contraseña = (string)$row['contraseña'];
		$usuario->role = (string)$row['role'];

        return $usuario;

    }

	public function getUsuarioIsLogin(string $email, string $pass):Usuario
    {
        $sql = "SELECT * FROM usuarios WHERE email='$email' AND contraseña=MD5('$pass')";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        
        
        $row = $statement->fetch();

        if (empty($row)) {
			$usuario = new Usuario();
			$usuario->dni = null;
			$usuario->nombre_usuario = null;
			$usuario->apellidos_usuario = null;
			$usuario->email = null;
			$usuario->contraseña = null;
			$usuario->role = null;
            return $usuario;
        }

        // Map array to data object
        $usuario = new Usuario();
        $usuario->dni = (string)$row['dni'];
        $usuario->nombre_usuario = (string)$row['nombre_usuario'];
        $usuario->apellidos_usuario = (string)$row['apellidos_usuario'];
        $usuario->email = (string)$row['email'];
        $usuario->contraseña = (string)$row['contraseña'];
		$usuario->role = (string)$row['role'];

        return $usuario;


    }
	
	public function getUsuarioByEmailApp(string $email):array
    {
        $sql = "SELECT * from usuarios WHERE email = :id;";
        $statement = $this->connection->prepare($sql);
        $statement->execute(['id' => $email]);
        
        
        $row = $statement->fetchAll();

        if (empty($row)) {
            throw new DomainException('No existe ningun registro de usuarios.', 404);
        }
        return $row ;

    }
	
	
	
	
	
	
	
	public function updateUsuarioApp(string $email, string $pass):string
    {
		
		$query ="UPDATE usuarios SET contraseña=MD5('".$pass."') WHERE email='".$email."';";
		
		$statement = $this->connection->prepare($query);
		$statement->bindParam('id', $dni);
		$statement->execute();

		return 'Se han actualizado los datos';
		
    }
	
}
