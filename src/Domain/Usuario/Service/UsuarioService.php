<?php

namespace App\Domain\Usuario\Service;

use App\Domain\Usuario\Data\Usuario;
use App\Domain\Usuario\Repository\UsuarioRepository;
use InvalidArgumentException;

/**
 * Service.
 */
final class UsuarioService
{
    /**
     * @var UsuarioRepository
     */
    private $repository;

    /**
     * The constructor.
     *
     * @param UsuarioRepository $repository The repository
     */
    public function __construct(UsuarioRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Create a new user.
     *
     * @param Usuario $user The user data
     *
     * @throws InvalidArgumentException
     *
     * @return int The new user ID
     */
    public function AddUsuario(Usuario $usuario): string
    {
        // Validation
        if (empty($usuario->dni)) {
            throw new InvalidArgumentException('DNI requerido.');
        }

        // Insert user
        $usuarioId = $this->repository->insertUsuario($usuario);

        // Logging here: Usuario created successfully

        return $usuarioId;
    }


    public function getUsuario():array
    {
        //     return $result;
        $usuarios = $this->repository->getUsuario();
        return $usuarios;
    }

    public function getUsuarioById(string $dni):Usuario
    {
        if(empty($dni)){
            throw new InvalidArgumentException('Usuario ID required');
        }

        $usuario = $this->repository->getUsuarioById($dni);
        return $usuario;
    }
	
	public function getUsuarioIsLogin(string $email, string $pass):Usuario
    {
        if(empty($email)){
            throw new InvalidArgumentException('Email Usuario requerido.');
        }
		
		if(empty($pass)){
            throw new InvalidArgumentException('Contraseña Usuario requerida.');
        }

        $usuario = $this->repository->getUsuarioIsLogin($email, $pass);
        return $usuario;
    }

    public function deleteUsuarioById(string $dni):string
    {
        if(empty($dni)){
            throw new InvalidArgumentException('ID Usuario requerida.');
        }

        $result = $this->repository->deleteUsuario($dni);
        return $result;
    }   

    public function updateUsuario(Usuario $usuario):string
    {
        if(empty($usuario)){
            throw new InvalidArgumentException(' Usuario requerido.');
        }

        $result = $this->repository->updateUsuario($usuario);
        return $result;
    }  
	
	public function updateUsuarioApp(string $email, string $pass):string
    {
        $result = $this->repository->updateUsuarioApp($email, $pass);
        return $result;
    }  
	
	public function getUsuarioByEmailApp(string $email):array
    {
        if(empty($email)){
            throw new InvalidArgumentException('Usuario ID required');
        }

        $usuario = $this->repository->getUsuarioByEmailApp($email);
        return $usuario;
    }
}